package jp.co.kenshu.mapper;

import java.sql.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.kenshu.entity.CommentText;

public interface CommentTextMapper {
	//全件取得
	List<CommentText> getCommentTextAll();

	//データベースにコメント追加
	//SpringのMapperでは引数二個入れる時は自動で色々されてしまうので@Paramつける決まり
	int insertCommentText(@Param("comment")String comment, @Param("messageId")int messageId,
		@Param("createdDate")Date createdDate, @Param("updatedDate")Date updatedDate);

	//コメントをアップデートした日時を更新
	int updatedComment(int messageId);

	//コメント削除
	//is_stoppedを使って表面上コメント削除(データベースから削除しない)
	//削除ボタン押されたときに1をデータベースに挿入
	int commentDelete(@Param("id")int id, @Param("isStopped")int isStopped);
}

