package jp.co.kenshu.controller;

import java.util.List;

import javax.validation.Valid;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.dto.text.CommentTextDto;
import jp.co.kenshu.dto.text.TextDto;
import jp.co.kenshu.form.CommentTextForm;
import jp.co.kenshu.form.TextForm;
import jp.co.kenshu.service.TextService;

@Controller
public class TextController {

	@Autowired
    private TextService textService;

	//データベースに既にある投稿を一件取得
	//http://localhost:8080/20ch/20ch/1でホーム

	//value = "/20ch/"はconfigと連動
    @RequestMapping(value = "/20ch/{id}", method = RequestMethod.GET)
    public String text(Model model, @PathVariable int id) {
        TextDto text = textService.getText(id);
        //このsentenceはtext.jspに対応
        model.addAttribute("sentence", "投稿してちょ");
        model.addAttribute("text", text);
        return "text";//text.jspに入っていく
    }

	//データベースの投稿を全件取得
    //http://localhost:8080/20ch/20ch/

    //value = "/20ch/"はconfigと連動
    @RequestMapping(value = "/20ch/", method = RequestMethod.GET)
    public String textAll(Model model) {
        List<TextDto> texts = textService.getTextAll();
        //このsentenceはtextAll.jspに対応
        model.addAttribute("sentence", "投稿してちょ");
        model.addAttribute("texts", texts);

        //データベースのコメントを全件取得して表示
        List<CommentTextDto> commentTexts = textService.getCommentTextAll();
        //このsentenceはtextAll.jspに対応
        model.addAttribute("comments", commentTexts);

        //コメント入力フォームを表示(投稿全件取得の画面に表示させるからここ)
        CommentTextForm commentForm = new CommentTextForm();
        model.addAttribute("commentTextForm", commentForm);

        //投稿消去時isStoppedの値を受け取る用Form
        //画面を表示させる用意として受け取るFormを作る
        TextForm messageDelete = new TextForm();
		model.addAttribute("messageDeleteForm", messageDelete);

		//コメント消去時isStoppedの値を受け取る用Form
        //画面を表示させる用意として受け取るFormを作る
        CommentTextForm commentDelete = new CommentTextForm();
		model.addAttribute("commentDeleteForm", commentDelete);

        return "textAll";
    }

    //投稿をデータべースに追加
    //http://localhost:8080/20ch/20ch/insert/input/

    @RequestMapping(value = "/20ch/insert/input/", method = RequestMethod.GET)
    public String textInsert(Model model) {
        TextForm form = new TextForm();
        model.addAttribute("textForm", form);
        model.addAttribute("sentence", "投稿を入力してちょ");
        model.addAttribute("sentence1", "どんどん投稿してちょ");
        return "textInsert";
    }

    @RequestMapping(value = "/20ch/insert/input/", method = RequestMethod.POST)
    public String textInsert(@Valid @ModelAttribute TextForm form, BindingResult result, Model model) {

        //投稿入力時のバリデーション
        if (result.hasErrors()) {
            model.addAttribute("title1", "エラー");
            model.addAttribute("message1", "以下のエラーを解消してください");
            return "textInsert";//Postされたら20chに遷移されますよ
        } else {
        	//投稿をアップデートした日時を追加
	        int count = textService.insertText(form.getMessage(), form.getCreatedDate(), form.getUpdatedDate());
	        Logger.getLogger(TextController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
	        return "redirect:/20ch/";//Postされたら20chに遷移されますよ
        }
    }

    //----------------------------------------------------------------------------------------------------------

    //コメントをデータベースに追加
    //(コメントの全件取得表示は、投稿の全件取得表示画面で行われるのでここには書かない)

    @RequestMapping(value = "/20ch/", method = RequestMethod.POST)
    public String commentTextInsert(@Valid @ModelAttribute CommentTextForm commentForm, BindingResult result, Model model) {

    	//コメント入力時のバリデーション
        if (result.hasErrors()) {
            model.addAttribute("title2", "エラー");
            model.addAttribute("message2", "コメントは1文字以上100文字以下で入力して下さい");

            //GETの内容をこっちに持ってきて無理やり表示させた！

            List<TextDto> texts = textService.getTextAll();
            //このsentenceはtextAll.jspに対応
            model.addAttribute("sentence", "投稿してちょ");
            model.addAttribute("texts", texts);

            //データベースのコメントを全件取得して表示
            List<CommentTextDto> commentTexts = textService.getCommentTextAll();
            //このsentenceはtextAll.jspに対応
            model.addAttribute("comments", commentTexts);

            //コメント入力フォームを表示(投稿全件取得の画面に表示させるからここ)
            CommentTextForm commentForm1 = new CommentTextForm();
            model.addAttribute("commentTextForm", commentForm1);

            return "textAll";
        } else {
        	//コメントをアップデートした日時を追加
	    	int count1 = textService.insertCommentText(commentForm.getComment(), commentForm.getMessageId(), commentForm.getCreatedDate(), commentForm.getUpdatedDate());
	    	int count2 = textService.updatedComment(commentForm.getMessageId());
	    	Logger.getLogger(TextController.class).log(Level.INFO, "挿入件数は" + count1 + "件です。");
	    	Logger.getLogger(TextController.class).log(Level.INFO, "挿入件数は" + count2 + "件です。");
	        return "redirect:/20ch/";//Postされたら20chに遷移されますよ
        }

    }

    //投稿削除

    //formを使ってjspからis_stopped=1を受け取り
  	//is_stoppedを使って表面上投稿削除(データベースから削除しない)
  	//削除ボタン押されたときに1をデータベースに挿入
    @RequestMapping(value = "/20ch/", params="messageDelete", method = RequestMethod.POST)
    public String messageDelete(@ModelAttribute TextForm form, Model model) {

    	//@ModelAttributeで以下の内容は自動で処理してくれているのでここには書かない
    	//TextForm messageDelete = new TextForm();
		//model.addAttribute("messageDeleteForm", messageDelete);

    	int count3 = textService.messageDelete(form.getId(), form.getIsStopped());
        Logger.getLogger(TextController.class).log(Level.INFO, "削除件数は" + count3 + "件です。");
        return "redirect:/20ch/";//Postされたら20chに遷移されますよ
    }

    //コメント削除
    @RequestMapping(value = "/20ch/", params="commentDelete", method = RequestMethod.POST)
    public String commentDelete(@ModelAttribute CommentTextForm form, Model model) {

    	int count4 = textService.commentDelete(form.getId(), form.getIsStopped());
        Logger.getLogger(TextController.class).log(Level.INFO, "削除件数は" + count4 + "件です。");
        return "redirect:/20ch/";//Postされたら20chに遷移されますよ
    }
}
