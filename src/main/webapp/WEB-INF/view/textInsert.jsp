<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<meta charset="utf-8">
<title>Welcome</title>
</head>
<body>
	<!-- 投稿をデータベースに追加 -->
    <h1>${sentence}</h1>
    <h2>${sentence1}</h2>

	<!-- バリデーション -->
	<h1>
       <c:out value="${title1}"></c:out>
    </h1>

    <p>
       <c:out value="${message1}"></c:out>
    </p>

    <form:form modelAttribute="textForm">

	    <!-- バリデーション用form:errorsタグの追加 -->
	    <!-- path=以降はFormと対応 -->
	    <div><form:errors path="message"  /></div>

	    <!-- setter,getterのmessage -->
        <form:textarea path="message" cols="100" rows="5"/>
        <input type="submit" value="投稿しちゃうヨ">
    </form:form>
</body>
</html>